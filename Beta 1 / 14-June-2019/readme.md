### BETA VERSION 1.0

- Internship Task in Telkom Indonesia Jakarta Selatan (Kebayoran Baru);
- Extracting Text in a Log File.;
- Exported to MySQL(convert from SQLite, source from http://stackoverflow.com/questions/18671/quick-easy-way-to-migrate-sqlite3-to-mysql
- Using .bat file to Run All Python Files in One Time;


# How To Use:

- Open "RUN.bat"
- Type file name to be process (Sample Log file not include to this repo), press Enter
- Type the name for DB 
- and you will have "DataPotensi.sql" as a Final Result of this program


# Pre-Requisite

- Python 3.7 has been install on your machine and make sure your PATH for python automatically added in your Environment Variable System
- Install SQLite to your machine (Follow this instruction : http://www.sqlitetutorial.net/download-install-sqlite/)
- Sometimes, you must kill any Python process if Final Result is not work (Kill in Task Manager)

# Library Requirement
- xls2db
- xlwt
- sqlite3
- re
- install that using PIP, google it for using pip in windows.
