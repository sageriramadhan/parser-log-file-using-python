import re
filename = input("Enter Filename: ")
clean_lines = []
with open(filename, "r") as f:
    lines = f.readlines()

lines = [line.replace(' ', '') for line in lines]

with open("Trial2.log", "w") as f:
    f.writelines('\n'.join(lines))
