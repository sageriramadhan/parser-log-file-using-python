### BETA VERSION 2.0

- Internship Task in Telkom Indonesia Jakarta Selatan (Kebayoran Baru);
- In this version, you can choose program to run Whole Files in one time (Just Select a folder, then all files will proceed), or run for one by one file
- Both of them, are using Windows Dialogs imported from Tkinter (GUI Python), no more copy paste name file or manually copy path folder/file

# How To Use (Run by One File):

- Open "main.bat" you want
- Select File from Windows Dialogs (Sample Log file not include to this repo), press Enter
- Type the name for Node 
- and you will have "DataPotensi.sql" as a Final Result of this program

# How To Use (Run Whole File from Selected Folder):

- Open "main_with_merge.bat" you want
- Select Folder from Windows Dialogs (Sample Log file not include to this repo), press Enter
- Type the name for Node 
- and you will have "DataPotensi.sql" as a Final Result of this program

# CAUTION!!!

- For main_with_merge, its take more times because the high workload,
- I'm using Intel i7 Gen8th with 8Gigs RAM, when i import the sql to PhpMyAdmin, it need 4 miinutes to done.
- PhpMyAdmin have threshold for execution time, which can only process for 5 minutes. if more than that, then automatically terminated.

# Pre-Requisite

- Python 3.7 has been install on your machine and make sure your PATH for python automatically added in your Environment Variable System
- Install SQLite to your machine (Follow this instruction : http://www.sqlitetutorial.net/download-install-sqlite/)
- Sometimes, you must kill any Python process if Final Result is not work (Kill in Task Manager)

# Library Requirement
- xls2db
- xlwt
- sqlite3
- re
- tkinter
- shutil

- install that using PIP, google it for using pip in windows.
