import os
import tkinter
from tkinter import filedialog
import shutil
import time
import datetime

print("Silahkan Pilih Kembali File Log Sebelumnya ...")
root = tkinter.Tk()
root.withdraw()
src = filedialog.askopenfilename()
source = os.path.dirname(os.path.abspath(src))
node = os.path.split(os.path.dirname(src))[1]

draft = (os.stat(src).st_ctime )                  
fix = time.strftime('%m/%d/%Y',time.gmtime(os.path.getctime(src)))
format_str = '%m/%d/%Y'
date = datetime.datetime.strptime(fix, format_str)
pix = date.strftime('%B %d %Y')

with open("DataPotensi.sql","r+") as file :
  filedata = file.read()

# Replace the target string
filedata = filedata.replace('(`No`,`Description`,`Interface`,`AdminState`,`OperState`,`LastStateChange`,`TransceiverType`,`SerialNumber`,`PartNumber`,`OpticalCompliance`);','( `No` decimal(4,1) DEFAULT NULL, `Description` varchar(58) DEFAULT NULL, `Interface` varchar(7) DEFAULT NULL, `AdminState` varchar(4) DEFAULT NULL, `OperState` varchar(18) DEFAULT NULL, `LastStateChange` varchar(18) DEFAULT NULL, `TransceiverType` varchar(4) DEFAULT NULL, `SerialNumber` varchar(14) DEFAULT NULL, `PartNumber` varchar(16) DEFAULT NULL, `OpticalCompliance` varchar(12) DEFAULT NULL );')

# Write the file out again
with open("DataPotensi.sql","w+") as file:
  file.write(filedata)

##########################################
with open("DataPotensi.sql","r+") as file :
  filedata = file.read()

# Replace the target string
filedata = filedata.replace(pix,'`' + pix + '`')
# Write the file out again
with open("DataPotensi.sql","w+") as file:
  file.write(filedata)

###############################################
with open("DataPotensi.sql","a+") as file:
    file.write("ALTER TABLE `%s` ADD `Node` VARCHAR(50) NULL DEFAULT NULL;\n" %(pix))
    file.write("UPDATE `%s` SET `Node` = '%s';\n" %(pix,node))

###############################################

with open("DataPotensi.sql","r+") as file :
  filedata = file.read()

# Replace the target string

filedata = filedata.replace('``' + pix + '``','`' + pix + '`')
# Write the file out again
with open("DataPotensi.sql","w+") as file:
  file.write(filedata)

##########
os.remove("pix")
os.remove("Trial2.log")
os.remove("Dataset.txt")

#########
db = 'Result.db'
sql = 'DataPotensi.sql'

shutil.move(db,source+'/'+db)
shutil.move(sql,source+'/'+sql)

