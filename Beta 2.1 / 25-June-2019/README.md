### BETA VERSION 2.1

- Internship Task in Telkom Indonesia Jakarta Selatan (Kebayoran Baru);
- In this version, you run for one by one file
- For Efficiency using Windows Dialogs imported from Tkinter (GUI Python), no more copy paste name file or manually copy path folder/file

# How To Use:

- Open "RUN.exe" you want
- Select File Log from Windows Dialogs (Sample Log file not include to this repo), press Enter
- Re-Select File Log
- The Final Result is in the Directory which File Log come from.

# Pre-Requisite

- Python 3.7 has been install on your machine and make sure your PATH for python automatically added in your Environment Variable System
- Install SQLite to your machine (Follow this instruction : http://www.sqlitetutorial.net/download-install-sqlite/)
- Sometimes, you must kill any Python process if Final Result is not work (Kill in Task Manager)

# Library Requirement
- xls2db
- xlwt
- sqlite3
- re
- tkinter
- shutil

- install that using PIP, google it for using pip in windows.
